import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;

class WheelOfFortune{
    public static void main(String[] args) {

        BufferedImage image1;
        BufferedImage image2;
        try{
            long timer = System.currentTimeMillis();
            image1 = new Robot().createScreenCapture(new Rectangle(Toolkit.getDefaultToolkit().getScreenSize()));
            image2 = new Robot().createScreenCapture(new Rectangle(Toolkit.getDefaultToolkit().getScreenSize()));
            timer = System.currentTimeMillis() - timer;
            System.out.println(timer);
            ImageIO.write(image2, "png", new File("screenshots" + File.separator + "screenshot2.png"));
            ImageIO.write(image1, "png", new File("screenshots" + File.separator + "screenshot1.png"));
        }
        catch(Exception e){
            System.out.println("Go home!");
        }


        HolHorse.compare("screenshots" + File.separator + "screenshot1.png", "screenshots" + File.separator + "screenshot2.png");
    }
}